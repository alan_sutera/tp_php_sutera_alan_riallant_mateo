CREATE DATABASE IF NOT EXISTS exo_blog;

CREATE USER IF NOT EXISTS post_user@localhost IDENTIFIED BY 'P0stW0rD';

GRANT ALL ON exo_blog.* TO post_user@localhost; 

USE exo_blog; 

CREATE TABLE IF NOT EXISTS users(
    id INT PRIMARY KEY AUTO_INCREMENT, 
    email VARCHAR(255) UNIQUE,
    password VARCHAR(255)
) engine=InnoDB; 

CREATE TABLE IF NOT EXISTS posts(
    id INT PRIMARY KEY AUTO_INCREMENT, 
    post TEXT NOT NULL,
    user_id INT NOT NULL, 
    FOREIGN KEY (user_id) REFERENCES users(id)
) engine=InnoDB; 

