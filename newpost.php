<?php
include('article.php');

if(session_status() != PHP_SESSION_ACTIVE) {
    session_start();
}



filter_input_array(
    INPUT_POST,
    [
        "contenu" => FILTER_SANITIZE_STRING,
    ]
);

$post = $_POST["contenu"];
$email = $_SESSION['email'];

include('db.php');
$cnx = new PDO('mysql:host=localhost;dbname=exo_blog', 'root', '');

$stmt = $cnx->prepare("SELECT id FROM users WHERE email= '$email'");
$stmt->execute();
$id = $stmt->fetchColumn();
$rqtIns = <<<SQL
            INSERT INTO posts (post, user_id)
            VALUES (:post, :user_id);
        SQL;

    $stmtInsert = $cnx->prepare($rqtIns);
    $stmtInsert->bindParam(":post", $post, PDO::PARAM_STR);
    $stmtInsert->bindParam(":user_id", $id, PDO::PARAM_INT);
    $stmtInsert->execute();

    header("location:article.php");