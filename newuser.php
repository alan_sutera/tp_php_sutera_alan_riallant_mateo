<?php

filter_input_array(
    INPUT_POST,
    [
        "email" => FILTER_SANITIZE_EMAIL,
        "motdepasse" => FILTER_SANITIZE_STRING,
        "motdepassebis" => FILTER_SANITIZE_STRING
    ]
);


$email = $_POST["email"];
$mdp = $_POST["motdepasse"];
$mdpbis = $_POST["motdepassebis"];

if ($mdp === $mdpbis) {
    $hash = password_hash($mdp, PASSWORD_DEFAULT);
}else{
    header("location:newusererror.php");
    exit;
}

include('db.php');
$cnx = new PDO('mysql:host=localhost;dbname=exo_blog', 'root', '');

$stmt = $cnx->prepare("SELECT * FROM users WHERE email=?");
$stmt->execute([$email]);
$user = $stmt->fetch();
if ($user) {
    header("location:newusererror.php");
    exit;
} else {
    $rqtIns = <<<SQL
            INSERT INTO users (email, password)
            VALUES (:email, :motdepasse);
        SQL;

    $stmtInsert = $cnx->prepare($rqtIns);
    $stmtInsert->bindParam(":email", $email, PDO::PARAM_STR);
    $stmtInsert->bindParam(":motdepasse", $hash, PDO::PARAM_STR_CHAR);
    print($hash);
    print($email);

    $stmtInsert->execute();
    if(session_status() != PHP_SESSION_ACTIVE) {
        session_start();
        $_SESSION['email'] = $_POST ["email"];
    }
    header("location:article.php"); 
}