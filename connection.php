<?php

filter_input_array(
    INPUT_POST,
    [
        "email" => FILTER_SANITIZE_EMAIL,
        "motdepasse" => FILTER_SANITIZE_STRING,
    ]
);

$email = $_POST["email"];
$mdp = $_POST["motdepasse"];


include('db.php');
$cnx = new PDO('mysql:host=localhost;dbname=exo_blog', 'root', '');

$stmt = $cnx->prepare("SELECT * FROM users WHERE email=?");
$stmt->execute([$email]);
$user = $stmt->fetch();
if ($user) {
    $stmt = $cnx->prepare("SELECT PASSwORD FROM users WHERE email='$email'");
    $stmt->execute();
    $pass = $stmt->fetchColumn();
    if (password_verify($mdp ,$pass )){
        session_start();
        $_SESSION['email'] = $_POST ["email"];
        header("location:article.php");
        exit;
    }else{

        header("location:connecterror.php");
        exit;
    }
}else{
    header("location:connecterror.php");
        exit;
}
