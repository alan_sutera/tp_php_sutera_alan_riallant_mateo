# TP_php_SUTERA_Alan_RIALLANT_Mateo

# Éxercice

Votre objectif est de réaliser un mini-site de blog. 
Un utilisateur pourra : 

- s'enregistrer
- se connecter
- ajouter un post (en étant connecté)
- visualiser sa liste de post (en étant connecté)
- se déconnecter

## Consignes : 

- vous enverrez par mail un lien github/gitlab public, contenant l'ensemble de vos fichiers
- Le présent fichier sera à la racine de votre git, nommé README.md. Vous y aurez cocher les cases des fonctionnalités réalisées, en indiquant à côté de la case à cocher le prénom de la personne qui à réalisé l'étape. 

## Groupe : 

RIALLANT Mateo
SUTERA Alan

## Étapes : 

### Enregistrement : 

 - [X] MATTEO Front
   - [X] MATTEO formulaire d'enregistrement :
     - [X] MATTEO email
    <!--  - [ ] MATTEO pseudo -->
     - [X] MATTEO mot de passe
     - [X] MATTEO confirmation du mot de passe
 - [X] MATTEO back :
   - [X] MATTEO validation et nettoyage des données saisies
   - [X] MATTEO Connexion à la base de données
     - [X] MATTEO vérification que l'email <!-- ou le pseudo --> ne sont pas déjà utilisés
   - [X] MATTEO vérification que les mots de passes saisis sont identiques (mot de passe et mot de passe de confirmation)
   - [X] MATTEO Si tout OK, 
     - [X] MATTEO hash du mot de passe
     - [X] MATTEO enregistrement de l'utilisateur dans la base, avec mot de passe hashé
     - [X] MATTEO création de session
     - [X] MATTEO redirection sur la page de saisie de post
   - [X] MATTEO Si erreur, affichage de message d'erreur, et d'un lien pour revenir à l'enregistrement

### Login 

 - [X] MATTEO Front
   - [X] MATTEO formulaire de connexion :
     - [X] MATTEO un champ email
     - [X] MATTEO un champ mot de passe
 - [X] MATTEO Back 
   - [X] MATTEO Validation et récupération des données
   - [X] MATTEO Recherche de l'utilisateur dans la base de donnée
     - [X] MATTEO Si trouvé, vérification du mot de passe
       - [X] MATTEO Si ok, création de la session, et redirection sur la page des posts
   - [X] MATTEO Si erreur, affichage de message d'erreur, et d'un lien pour revenir à la connexion

## Affichage des posts : 

 - [ ] ALAN Vérification de l'état de connexion de l'utilisateur 
   - [ ] ALAN Si connecté, 
     - [ ] ALAN recherche de tous les posts de l'utilisateur en base de données
     - [X] ALAN parcours et affichage des posts
     - [X] ALAN affichage de liens
       - [X] ALAN vers la saisie d'un post
       - [X] ALAN pour se déconnecter
   - [X] ALAN si non connecté : redirection vers la page de connexion

## Saisie d'un post

 - [ ] ALAN Front (PHP + html): 
   - [X] ALAN si utilisateur connecté
     - [X] ALAN formulaire de saisie du post + bouton d'envoi
     - [X] ALAN lien de retour à l'affichage des posts de l'utilisateur
   - [ ] ALAN Si non connecté : redirection vers la page de connexion
 - [X] ALAN Back : 
   - [X] ALAN Validation et récupération des données
   - [X] ALAN Connexion à la BDD
   - [X] ALAN Préparation de la requête d'insertion du post
   - [X] ALAN association des paramètres
   - [X] ALAN execution de la requête
   - [X] ALAN Si tout s'est bien passé, retour à la liste des posts